﻿using dotnet_ef_core.DataContext;
using Microsoft.EntityFrameworkCore;

namespace dotnet_ef_core.DataContext
{
    
    public class ApplicationDbContext : DbContext
    {

        //public ApplicationDbContext()
        //{

        //}

        //public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        //{
        //}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;database=ef_db;uid=root;pwd=1r2e3z4a;");
        }

        public DbSet<CategoryEntity> categories { get; set; }

    }
}
